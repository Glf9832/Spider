﻿#_*_coding:utf-8_*_

# Author: Glf

from openpyxl import Workbook
from openpyxl import load_workbook

class Execl(object):
    def CreateExecl(self,fileName):
        '''创建Execl工作薄'''
        try:
            wb = Workbook()
            wb.save(fileName)
            return wb
        except Exception,e:
            print u'创建失败,位置：CreateExecl(),错误信息:',e

    def load_sheet(self,workbook,sheetName):
        '''工作表对象'''
        try:
            sheet = workbook.get_sheet_by_name(sheetName)
            return sheet
        except Exception,e:
            print u"工作表出错,位置：SheetObject(),错误信息：",e

    def WriteValue(self,sheetName,row,column,value):
        '''向指定表中插入数据'''
        try:
            sheetName.cell(row=row,column=column).value = value
        except Exception,e:
            print u"插入数据出错,位置：WriteValue(),错误信息：",e

    def ReadValue(self,sheetName,row,column):
        '''读取指定位置数据'''
        try:
            return sheetName.cell(row=row,column=column).value
        except Exception,e:
            print u"读取出错,位置：ReadValue(),错误信息：",e

    def GetRowCount(selt,sheetName):
        '''获取工作表行数'''
        try:
            return sheetName.max_row
        except Exception,e:
            print u'获取行数失败,位置：GetRowCount(),错误信息：',e

    def GetColumnCount(selt,sheetName):
        '''获取工作表列数'''
        try:
            return sheetName.max_column
        except Exception,e:
            print u'获取列数失败,位置：GetColumnCount(),错误信息：',e

if __name__ == "__main__":
    print "This is Execl_Module"

'''
用法示例：
fileName = u"Test.xlsx"
execl = Execl()
workbook = load_workbook(fileName)
sheet = execl.load_sheet(workbook,"Sheet")
valuelist = ['Id','Title','Url']
i = 1
for value in valuelist:
    execl.WriteValue(sheet,1,i,value)
    i = i + 1
workbook.save(fileName)
'''









