#_*_coding:utf-8_*_

# Author: Glf

import requests
from bs4 import BeautifulSoup
from Execl import Execl
from openpyxl import Workbook
from openpyxl import load_workbook

class wynewSplider(object):
    def __init__(self,url,execlName):
        self.url = url
        self.execlName = execlName

    def GetHtml(self,url):
        html = requests.get(url).text
        return html

    def GetClassLinks(self):
        '''抓取新闻分类链接'''
        html = self.GetHtml(self.url)
        soup = BeautifulSoup(html,'html.parser')
        subNavs = soup.find_all('div',attrs={'class':'subNav'})
        i = 2
        id = 1
        execl = Execl()
        workbook = load_workbook(self.execlName)
        sheet_classLinks = execl.load_sheet(workbook,'classLinks') #载入工作表

        print u'正在抓取新闻分类链接,请稍后......'
        try:
            for subNav in subNavs:
                for a_Tag in subNav.find_all('a'):
                    execl.WriteValue(sheet_classLinks,i,1,id)
                    execl.WriteValue(sheet_classLinks,i,2,a_Tag.text)         #保存新闻分类名
                    execl.WriteValue(sheet_classLinks,i,3,a_Tag.get('href'))  #保持链接
                    i += 1
                    id += 1
            workbook.save(self.execlName)
            print u'抓取完成,共{0}条分类'.format(id)
        except Exception,e:
            print u'位置：GetClassLinks(),错误信息：',e

    def GetNewsLink(self):
        '''抓取新闻链接'''
        execl = Execl()
        workbook = load_workbook(self.execlName)
        sheet_classLinks = execl.load_sheet(workbook,'classLinks')
        sheet_newLinks = execl.load_sheet(workbook,'newLinks')
        sheetRowCount = execl.GetRowCount(sheet_classLinks)
        n = 2
        id = 1
        print u'正在抓取新闻链接，请稍后......'
        try:
            for i in xrange(2,sheetRowCount):
                classLink = execl.ReadValue(sheet_classLinks,i,3)
                html = self.GetHtml(classLink)
                soup = BeautifulSoup(html,'html.parser')
                div_Tags = soup.find_all('div',attrs={'class':'area areabg1'})
                for td_Tags in div_Tags:
                    for tr_Tag in td_Tags.find_all('tr'):
                        if tr_Tag.a is not None:
                            execl.WriteValue(sheet_newLinks,n,1,id)
                            execl.WriteValue(sheet_newLinks,n,2,tr_Tag.find('a').text)
                            execl.WriteValue(sheet_newLinks,n,3,tr_Tag.find('a').get('href'))
                            if tr_Tag.find('td',attrs={'class':'cBlue'}).text.isdigit():
                                execl.WriteValue(sheet_newLinks,n,4,int(tr_Tag.find('td',attrs={'class':'cBlue'}).text))
                            else:
                                execl.WriteValue(sheet_newLinks,n,4,tr_Tag.find('td',attrs={'class':'cBlue'}).text)
                            n += 1
                            id += 1
            workbook.save(self.execlName)
            print u'抓取完成,共{0}条新闻'.format(id - 1)
        except Exception,e:
            print u'位置：错误信息：',e

if __name__ == '__main__':
    print u'网易新闻排行榜爬虫类'
    
    url_wy = 'http://news.163.com/rank/'
    execlName = 'wyNews.xlsx'

    wyNew = wynewSplider(url_wy,execlName)
    wyNew.GetClassLinks()
    wyNew.GetNewsLink()


'''
使用示例：
url_wy = 'http://news.163.com/rank/'
execlName = 'wyNews2.xlsx'

wyNew = wynewSplider(url_wy,execlName)
wyNew.GetClassLinks()
wyNew.GetNewsLink()
'''









