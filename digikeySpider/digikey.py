#_*_coding:utf-8_*_

import requests
from bs4 import BeautifulSoup

def GetHtml(url):
    '''获取网页源码'''
    try:
        html = requests.get(url).text
        return html
    except Exception,e:
        print u'抓取错误，错误信息：{0}'.format(e)

def Savefile(filepath,file):
    '''将内容保存到指定文件'''
    with open(filepath,'a') as fp:
        fp.write(file)
        fp.close()

def Getlink(urlone,urltwo,onetext,twotext):
	'''获取产品页链接'''
	html1 = GetHtml(urlone)
	soup1 = BeautifulSoup(html1,"html.parser")
	link1 = soup1.find('a',text=onetext).get('href')
	link1 = urltwo+link1
	html2 = GetHtml(link1)
	soup2 = BeautifulSoup(html2,"html.parser")
	link2 = soup2.find('a',text=twotext).get('href')
	link2 = urltwo+link2
	return link2

def Getlinklist(link):
		'''获取产品列表链接'''
		html3 = GetHtml(link).encode('utf-8')
		soup3 = BeautifulSoup(html3,"html.parser")
		link3 = soup3.find_all('td',attrs={'class':'tr-image'})
		return link3

def GetPage(link):
	'''获取产品信息'''
	try:
		for ul in link:
			ul.a
			link4 = ul.find('a').get('href')
			link4 = url + link4
			# print link4
			html4 = GetHtml(link4)
			soup4 = BeautifulSoup(html4, "html.parser")
			for href in soup4.find_all('div', attrs={'class': 'breadcrumbs'}):
				for productlink in href.find_all('a'):
					Savefile(filePath,(productlink.text.encode('utf-8') + '  ' + (url + productlink.get('href')).encode('utf-8')) + '\n')
			try:
				Savefile(filePath,'产品图片：  ' + 'http:' + soup4.find('div', attrs={'id': 'product-photo-wrapper'}).a.img.get('src').encode('utf-8') + '\n')
			except:
				print '无图片！'

			tbody = soup4.find('table', attrs={'id': 'product-details'})
			for tr in tbody.find_all('tr'):
				Savefile(filePath,tr.th.text.strip().encode('utf-8') + '  ' + tr.td.text.strip().encode('utf-8') + '\n')
			media = soup4.find('span', attrs={'class': 'more-expander-item'})
			try:
				Savefile(filePath,'数据列表：' + 'http:' + media.a.get('href').encode('utf-8') + '\n')
			except:
				print '无数据列表！'
			finally:
				Savefile(filePath,'----------------------------------------------------------------------------\n\n\n\n')
			print '保存成功！'
	except Exception,e:
		print '出错！错误信息：',e

url_zh = 'http://www.digikey.cn/zh'
url = 'http://www.digikey.cn'
# filePath = './ProductInfo.txt'
filePath = './ProductInfo1.txt'

# link1 = Getlink(url_zh,url,"电感器，线圈，扼流圈","可调电感器")
link1 = Getlink(url_zh,url,"电容器","云母和 PTFE 电容器")
print "开始抓取第1页"
link2 = Getlinklist(link1)
GetPage(link2)

# for x in xrange(2,4):
for x in xrange(2,6):
	print "开始抓取第{0}页".format(x)
	link3 = Getlinklist(link1+'/page/{0}'.format(x))
	GetPage(link3)





























