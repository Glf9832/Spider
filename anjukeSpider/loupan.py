#_*_coding:utf-8_*_

'''
楼盘名：
地址：
动态：
价格：
联系电话：
'''

import requests
from lxml import html

class loupans(object):
    def GetSource(self,url):
        head = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
                'Host':'sz.fang.anjuke.com',
                'Request URL':'http://sz.fang.anjuke.com/loupan/'
                }
        htmls = requests.get(url,headers=head).content
        return htmls

    def GetLoupanInfo(self,htmlSource):
        selector = html.fromstring(htmlSource)
        infos = selector.xpath('//div[@class="key-list"]/div[@class="item-mod"]')
        Info = {}
        InfoList = []
        for info in infos:
            try:
                name = info.xpath('div[@class="infos"]/div[@class="lp-name"]/h3/a/text()')
                url = info.xpath('div[@class="infos"]/div[@class="lp-name"]/h3/a/@href')[0]
                address = info.xpath('div[@class="infos"]/p[@class="address"]/a/text()')
                act = info.xpath('div[@class="infos"]/div[@class="data-brief"]/a/text()')
                price = info.xpath('div[@class="favor-pos"]/p[@class="price"]/span/text()')
                tel = info.xpath('div[@class="favor-pos"]/p[@class="tel"]/text()')

                Info["楼盘名"] = self.IsHaveinfo(name)
                Info["链接"] = url
                Info["地址"] = self.IsHaveinfo(address)
                Info["动态"] = self.IsHaveinfo(act)
                Info["均价"] = self.IsHaveinfo(price)
                Info["联系电话"] = self.IsHaveinfo(tel)
                InfoList.append(Info)

            except Exception,ex:
                print ex

    def IsHaveinfo(self,info):
        if info !=[]:
            info = info[0]
        else:
            info = '暂无信息'
        return info

if __name__ == '__main__':
    url_loupan = 'http://sz.fang.anjuke.com/loupan/'
    loupan = loupans()
    selector = loupan.GetSource(url_loupan)
    loupan.GetLoupanInfo(selector)





















